//
//  KKAppDelegate.h
//  UUIDGen
//
//  Created by 张 琪 on 14-1-11.
//  Copyright (c) 2014年 张 琪. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KKAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
