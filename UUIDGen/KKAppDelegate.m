//
//  KKAppDelegate.m
//  UUIDGen
//
//  Created by 张 琪 on 14-1-11.
//  Copyright (c) 2014年 张 琪. All rights reserved.
//

#import "KKAppDelegate.h"

@interface KKAppDelegate()
@property (nonatomic) IBOutlet NSTextField* textview;
@end

@implementation KKAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [self gen];
}

- (void)gen{
    CFUUIDRef cfuuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString * str = CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, cfuuid));
    CFRelease(cfuuid);

    _textview.stringValue = str;
}

- (IBAction)onGenUUID:(id)sender{
    [self gen];
}

- (IBAction)onCopy:(id)sender{
    NSPasteboard * pb = [NSPasteboard generalPasteboard];
    [pb declareTypes:@[NSStringPboardType] owner:nil];
    [pb setString:_textview.stringValue forType:NSStringPboardType];
    
    _textview.alphaValue = 0;
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:0.5];
    _textview.animator.alphaValue = 1;
    [NSAnimationContext endGrouping];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender{
    return YES;
}

@end
